<?php

namespace Modules\Telegram;

use Symfony\Component\Console\Output\ConsoleOutput;

class Telegram
{
    protected $console;

    public function __construct()
    {
        $this->console = new ConsoleOutput();
    }

    /**
     * Enabling Telegram Module
     * @return boolean
     */
    public function enable()
    {
        // enable the module
        \Artisan::call("module:enable", [
            'module' => 'Telegram'
        ]);

        return true;
    }

    /**
     * Disabling Telegram Module
     * @return boolean
     */
    public function disable()
    {
        // Disable the module
        \Artisan::call("module:disable", [
            'module' => 'Telegram'
        ]);

        return true;
    }
}
