<?php

Route::group(['middleware' => 'web', 'prefix' => 'telegram', 'namespace' => 'Modules\Telegram\Http\Controllers'], function()
{
    Route::get('/', 'TelegramController@index');
});
