<?php

return [
    'name' => 'Telegram',
    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN', 'TOKEN_')
    ],
];
